console.log("The sum of numA and numB should be 5");
let numA = 2;
let numB = 3;
let sum = numA + numB;
console.log("Code Output: " + sum);
console.log("-------------");

console.log("The difference of numA minus numB should be 2");
numA = 7;
numB = 5;
let difference = numA - numB;
console.log("Code Output: " + difference);
console.log("-------------");

console.log("The product of numA times numB should be 8");
numA = 4;
numB = 2;
let product = numA * numB;
console.log("Code Output: " + product);
console.log("-------------");

console.log("The qoutient of numA divided by numB should be 3");
numA = 6;
numB = 2;
let qoutient = numA / numB;
console.log("Code Output: " + qoutient);
console.log("-------------");

